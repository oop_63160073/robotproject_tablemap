/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.robotproject_tablemap;

/**
 *
 * @author Acer
 */
public class Robot {

    private int x;
    private int y;
    private char symbol;
    private TableMap map;

    public Robot(int x, int y, char symbol, TableMap map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N':
            case 'w':
                if (walkNorth()) return false;
                break;
            case 'S':
            case 's':
                if (walkSouth()) return false;
                break;
            case 'W':
            case 'a':
                if (walkWest()) return false;
                break;
            case 'E':
            case 'd':
                if (walkEast()) return false;
                break;
        }
        foundBomb();
        return true;
    }

    private void foundBomb() {
        if(map.isBomb(x, y)){
            System.out.println("Founded Bomb!!!!");
        }
    }

    private boolean walkEast() {
        if (map.inMap(x + 1, y)) {
            x = x + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkWest() {
        if (map.inMap(x - 1, y)) {
            x = x - 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkSouth() {
        if (map.inMap(x, y + 1)) {
            y = y + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkNorth() {
        if (map.inMap(x, y - 1)) {
            y = y - 1;
        } else {
            return true;
        }
        return false;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }

}
