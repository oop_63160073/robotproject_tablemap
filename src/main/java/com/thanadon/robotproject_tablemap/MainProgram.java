/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.robotproject_tablemap;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        TableMap map = new TableMap(5,5);
        Bomb bomb = new Bomb(2,2);
        Robot robot = new Robot(4,1,'x',map);
        map.setBomb(bomb);
        map.setRobot(robot);
        while(true){
            map.showMap();
            char direction = inputDirection(kb);
            if(direction == 'q'){
                printBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printBye() {
        System.out.println("Bye Bye!!");
    }

    private static char inputDirection(Scanner kb) {
        String str = kb.next();
        char direction = str.charAt(0);
        return direction;
    }
}
